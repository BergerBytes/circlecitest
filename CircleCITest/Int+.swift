//
//  AddOne.swift
//  CircleCITest
//
//  Created by BergerBytes on 5/10/18.
//  Copyright © 2018 Bergerbytes. All rights reserved.
//

import Foundation

extension Int {
    mutating func AddOne() {
        self = self + 1
    }
}
