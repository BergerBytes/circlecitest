//
//  CircleCITestTests.swift
//  CircleCITestTests
//
//  Created by BergerBytes on 5/10/18.
//  Copyright © 2018 Bergerbytes. All rights reserved.
//

import XCTest
@testable import CircleCITest

class CircleCITestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddOne() {
        var testNumber = 1
        testNumber.AddOne()
        
        XCTAssertTrue(testNumber == 2)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
